
class Api::V1::UsersController < Api::V1::BaseController

    VALID_SAVE_KEYS = [:login, :password, :role].freeze

    VALID_VIEW_KEYS = [:login, :created_at, :updated_at].freeze

    def valid_save_keys
        VALID_SAVE_KEYS
    end

    def valid_view_keys
        VALID_VIEW_KEYS
    end

    module Actions

        def index
            return error_json("Must be admin", :forbidden) \
                if ! current_user.admin?

            allUsers = filtered_user_props(User.all) # NOTE: We'll need paging in future

            render json: { users: allUsers }
        end

        def show
            user = user_smart_find

            return error_json("Not found", :not_found) \
                if ! user

            return error_json("Must be admin", :forbidden) \
                if ! current_user.admin? && user.id != current_user.id

            render json: filtered_user_props([user]).first
        end

        def create
            return error_json("Must be admin", :forbidden) \
                if ! current_user.admin?

            jsonIn = request.body.read

            input = filter_save_keys(JSON.parse(jsonIn))

            #log.debug { "Creating new user: #{input.inspect}" } # Beware of logging passwords!
            newUser = User.new(input)
            newUser.save or return error_json("Failed to save", model: newUser)

            render json: newUser, status: :created # No filter needed, since only admin is here
        end

        def update
            user = user_smart_find

            return error_json("Not found", :not_found) \
                if ! user

            return error_json("Must be admin", :forbidden) \
                if ! current_user.admin? && user.id != current_user.id

            jsonIn = request.body.read

            input = filter_save_keys(JSON.parse(jsonIn))
            input.each { |prop, value| user.send("#{prop}=", value) }

            #log.debug { "Updating user: #{user.as_json}" } # Beware of logging passwords!
            user.save or return error_json("Failed to save", model: user)

            render json: filtered_user_props([user]).first, status: :accepted
        end

        def destroy
            not_implemented
        end

        def follow
            rowValues = { follower_id: current_user.id, user_id: params[:id].to_i }
            mapRow = FollowerRow.where(rowValues).first
            if ! mapRow
                mapRow = FollowerRow.new(rowValues)
                mapRow.save or return error_json("Failed to save")
            end

            render json: mapRow, status: :created
        end

        def unfollow
            rowValues = { follower_id: current_user.id, user_id: params[:id].to_i }
            mapRow = FollowerRow.where(rowValues).first
            if mapRow
                FollowerRow.where(rowValues).delete_all >= 1 or return error_json("Failed to save")
            end

            render nothing: true, status: :no_content
        end

        def following
            users = filtered_user_props(current_user.following)
            render json: { user_id: current_user.id, is_following: users }
        end

        def followers
            users = filtered_user_props(current_user.followers)
            render json: { user_id: current_user.id, has_followers: users }
        end

    end # Actions

    include Actions

protected

    def filtered_user_props(user_models)
        return user_models if current_user.admin?
        user_models.map { |u| filter_view_keys(u.as_json) }
    end

    def user_smart_find
        if params[:id] == "me"
            uid = current_user.id
        else
            uid = params[:id].to_i
        end
        User.find_by(id: uid)
    end

end # Api::V1::UsersController
