
class Api::V1::FeedController < Api::V1::BaseController

    # Embedded lambda here requires calling {#filter_keys} directly.
    IMAGE_FEED_VALID_KEYS = [:url, :caption, :likes, ->(image) { { owner: image.user.login } }, :created_at].freeze

    module Actions

        def my_feed
            windowSize  = (params[:count] || 20).to_i
            windowStart = (params[:start] || 0).to_i

            followedUserIds = User.fast_followed_ids(current_user.id)
            followersCount  = User.fast_followers_count(current_user.id)

            # Eager load to avoid N+1 queries to get User.login
            images = Image.includes(:user).order(created_at: :desc) \
                .offset(windowStart).limit(windowSize) \
                .where(user_id: followedUserIds) \
                .to_a
            images.map! { |image| filter_keys(image, IMAGE_FEED_VALID_KEYS) }

            render json: common_result_hasn(followersCount, windowStart, windowSize, images)
        end

        def popular_feed
            windowSize  = (params[:count] || 20).to_i
            windowStart = (params[:start] || 0).to_i

            followersCount = User.fast_followers_count(current_user.id)

            # Eager load to avoid N+1 queries to get User.login
            images = Image.includes(:user).order(likes: :desc, updated_at: :desc) \
                .offset(windowStart).limit(windowSize) \
                .where("updated_at >= ?", Time.current - 1.hour) \
                .to_a
            images.map! { |image| filter_keys(image, IMAGE_FEED_VALID_KEYS) }

            render json: common_result_hasn(followersCount, windowStart, windowSize, images)
        end

    end # Actions

    include Actions

protected

    def common_result_hasn(followersCount, windowStart, windowSize, images)
        {
            user_id:              current_user.id,
            user_followers_count: followersCount,
            feed_window_start:    windowStart,
            feed_window_size:     windowSize,
            images_feed:          images
        }
    end

end # Api::V1::FeedController
