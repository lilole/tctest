
class Api::V1::BaseController < ApplicationController

    protect_from_forgery with: :null_session

    before_action :destroy_session, :authenticate

    attr_reader :current_user # Set by {#authenticate}.

    ##
     # Really disable creating a session object after disabling CSRF with {.protect_from_forgery}.
     #
    def destroy_session
        request.session_options[:skip] = true
    end

    ##
     # For every request, check HTTP Basic authn creds.
     #
    def authenticate
        user = authenticate_with_http_basic { |login, passwd| User.authenticate(login, passwd) }
        if user
            @current_user = user
        else
            request_http_basic_authentication
        end
    end

    ##
     # Convenience method for any unimplemented actions, simply renders a `:not_implemented` response.
     #
    def not_implemented
        error_json("Not implemented: #{params.to_json}", :not_implemented)
    end

    ##
     # Responds with error JSON in one line.
     #
    def error_json(message, status=:bad_request, model: nil)
        if model && ! model.errors.empty?
            message = model.errors.full_messages.join("\n")
        end
        render json: { error: { message: message } }, status: status
    end

    ##
     # Convenient access to logger.
     #
    def log
        Rails.logger
    end

    ##
     # Controllers may override this to use the {#filter_save_keys} method. It must return a list of symbols in the whitelist to
     # be saved in the JSON request.
     #
    def valid_save_keys
        raise NotImplementedError, "You must implement this method in #{self.class.name}"
    end

    ##
     # Controllers may override this to use the {#filter_save_keys} method. It must return a list of symbols in the whitelist to
     # be returned in the JSON response.
     #
    def valid_view_keys
        raise NotImplementedError, "You must implement this method in #{self.class.name}"
    end

    ##
     # Use {#valid_save_keys} result to use only whitelisted keys in JSON to be saved.
     #
    def filter_save_keys(hash, valid_keys=nil)
        filter_keys(hash.symbolize_keys, valid_keys || valid_save_keys)
    end

    ##
     # User {#valid_view_keys} result to use only whitelisted keys in JSON to be returned.
     #
    def filter_view_keys(hash, valid_keys=nil)
        filter_keys(hash.symbolize_keys, valid_keys || valid_view_keys)
    end

    ##
     # Generic whitelist keys, accepts an input Hash or ApplicationRecord, and whitelist as symbols or Proc objects. Procs receive
     # the input ApplicationRecord and must return a Hash to add to the filtered result. The input CANNOT be a Hash if any
     # whitelist object is a Proc.
     #
    def filter_keys(symbol_key_hash_or_model, valid_key_symbols_or_procs)
        if symbol_key_hash_or_model.is_a?(ApplicationRecord)
            model = symbol_key_hash_or_model
            hash  = symbol_key_hash_or_model.as_json.symbolize_keys
        else
            model = nil
            hash  = symbol_key_hash_or_model
        end

        filteredHash = {}

        valid_key_symbols_or_procs.each do |key|
            if key.is_a?(Proc)
                model and filteredHash.merge!(key.call(model))
            else
                hash.key?(key) and filteredHash[key] = hash[key]
            end
        end

        filteredHash
    end

end
