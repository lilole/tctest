
class Api::V1::ImagesController < Api::V1::BaseController

    VALID_SAVE_KEYS = [:url, :caption, :user_id].freeze

    def valid_save_keys
        VALID_SAVE_KEYS
    end

    module Actions

        def index
            images = Image.where(user_id: current_user.id)
            render json: { images: images }
        end

        def show
            image = Image.find_by(id: params[:id])

            return error_json("Not found", :not_found) \
                if ! image

            return error_json("Not your image", :forbidden) \
                if ! current_user.admin? && image.user_id != current_user.id

            render json: image
        end

        def create
            jsonIn = request.body.read

            input = filter_save_keys(JSON.parse(jsonIn))
            input[:user_id] = current_user.id if ! input[:user_id] || ! current_user.admin?

            log.debug { "Creating new image: #{input.inspect}" }
            newImage = Image.new(input)
            newImage.save or return error_json("Failed to save")

            render json: newImage, status: :created
        end

        def update
            image = Image.find_by(id: params[:id])

            return error_json("Not found", :not_found) \
                if ! image

            return error_json("Not your image", :forbidden) \
                if ! current_user.admin? && image.user_id != current_user.id

            jsonIn = request.body.read

            input = filter_save_keys(JSON.parse(jsonIn), valid_save_keys - [:user_id])
            input.each { |prop, value| image.send("#{prop}=", value) }

            log.debug { "Updating image: #{image.as_json}" }
            image.save or return error_json("Failed to save")

            render json: image, status: :accepted
        end

        def destroy
            not_implemented
        end

        def like
            imageId = params[:id].to_i
            rowValues = { user_id: current_user.id, image_id: imageId }
            mapRow = UserImageLike.where(rowValues).first
            if ! mapRow
                Image.transaction do
                    # Add the like association
                    mapRow = UserImageLike.new(rowValues)
                    mapRow.save or return error_json("Failed to save")
                    # Increment the image's likes count
                    image = Image.find(imageId)
                    image.update(likes: image.likes + 1)
                end
            end

            render json: mapRow, status: :created
        end

        def unlike
            imageId = params[:id].to_i
            rowValues = { user_id: current_user.id, image_id: imageId }
            mapRow = UserImageLike.where(rowValues).first
            if mapRow
                Image.transaction do
                    # Remove the like association
                    UserImageLike.where(rowValues).delete_all >= 1 or return error_json("Failed to save")
                    # Decrement the image's likes count
                    image = Image.find(imageId)
                    image.likes > 0 and image.update(likes: image.likes - 1)
                end
            end

            render nothing: true, status: :no_content
        end

        def likes
            imageId = params[:id].to_i
            userIds = UserImageLike.where(image_id: imageId).pluck(:user_id)
            render json: { image_id: imageId, user_ids: userIds }
        end

    end # Actions

    include Actions

end # Api::V1::ImagesController
