
class FollowerRow < ApplicationRecord

    self.table_name = :followers

    belongs_to :user
    belongs_to :follower, class_name: :User

end # FollowerRow
