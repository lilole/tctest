
class User < ApplicationRecord

    has_many :user_follower_map, foreign_key: :user_id, class_name: :FollowerRow
    has_many :followers, through: :user_follower_map, class_name: :User, source: :follower

    has_many :user_following_map, foreign_key: :follower_id, class_name: :FollowerRow
    has_many :following, through: :user_following_map, class_name: :User, source: :user

    has_many :image_like_map, foreign_key: :user_id, class_name: :UserImageLike
    has_many :liked_images, through: :image_like_map, class_name: :Image, source: :image

    validates :login, uniqueness: true
    validates :role,  if: :role, inclusion: { in: %w[superadmin user] }

    def self.authenticate(login, passwd)
        User.find_by(login: login, encrypted_password: encrypt_password(passwd))
    end

    def self.encrypt_password(clear_text)
        # BCrypt would be safer, but requires an extra gem; skipping adding extra gems for now
        Digest::SHA2.new(512).base64digest(clear_text)
    end

    def self.fast_followed_ids(user_id)
        FollowerRow.where(follower_id: user_id).pluck(:user_id)
    end

    def self.fast_followers_count(user_id)
        FollowerRow.where(user_id: user_id).count
    end

    def password=(clear_text)
        self.encrypted_password = User.encrypt_password(clear_text)
    end

    def admin?(level=:super)
        case level
        when :super then role == "superadmin"
        # ...other builtin levels may be supported in future
        else role[-5,5] == "admin"
        end
    end

end # User
