
class CreateImages < ActiveRecord::Migration[5.0]

    def change
        create_table :images do |t|
            t.integer :user_id, null: false, index: true
            t.string  :url,     null: false
            t.string  :caption, null: true
            t.integer :likes,   null: false, default: 0, index: true

            t.timestamps index: true
        end

        add_foreign_key :images, :user
    end

end
