
class CreateUserImageLikes < ActiveRecord::Migration[5.0]

    def change
        create_table :user_image_likes, id: false do |t|
            t.integer :user_id,  null: false, index: true
            t.integer :image_id, null: false, index: true

            t.timestamps index: true
        end

        add_index :user_image_likes, [:user_id, :image_id], unique: true

        add_foreign_key :user_image_likes, :users,  column: :user_id
        add_foreign_key :user_image_likes, :images, column: :image_id
    end

end
