
class CreateFollowers < ActiveRecord::Migration[5.0]

    def change
        create_table :followers, id: false do |t|
            t.integer :user_id,     null: false, index: true
            t.integer :follower_id, null: false, index: true
        end

        add_index :followers, [:user_id, :follower_id], unique: true

        add_foreign_key :followers, :users, column: :user_id
        add_foreign_key :followers, :users, column: :follower_id
    end

end
