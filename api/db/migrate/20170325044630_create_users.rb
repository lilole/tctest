
class CreateUsers < ActiveRecord::Migration[5.0]

    def change
        create_table :users do |t|
            t.string :login,              null: false
            t.string :role,               null: false, default: 'user'
            t.string :encrypted_password, null: false

            t.timestamps
        end

        add_index :users, :login, unique: true
        add_index :users, :encrypted_password
    end

end
