# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

##
 # At least one real user to create real data with the endpoints.
 #
User.create [
    { login: "superadmin", password: (ENV["SA_PASSWORD"] || "tctest324"), role: "superadmin" }
]

if %w[development staging].member?(Rails.env)
    ## Add some test data to begin manually checking endpoints with Postman or similar.

    def id_for_login(login)
        User.where(login: login).pluck(:id).first
    end

    def id_for_url(url)
        Image.where(url: url).pluck(:id).first
    end

    # User 1
    User.create [{ login: "user1", password: "user1" }]
    Image.create [{ user_id: id_for_login("user1"), url: "http://user1", likes: 1 }] # Likers: User 2

    # User 2
    User.create [{ login: "user2", password: "user2" }]
    Image.create [{ user_id: id_for_login("user2"), url: "http://user2a", likes: 2 }] # Likers: User 3, 4
    Image.create [{ user_id: id_for_login("user2"), url: "http://user2b", likes: 1 }] # Likers: User 1

    # User 3
    User.create [{ login: "user3", password: "user3" }]
    Image.create [{ user_id: id_for_login("user3"), url: "http://user3", likes: 3 }] # Likers: User 1, 2, 4

    # User 4
    User.create [{ login: "user4", password: "user4" }]
    Image.create [{ user_id: id_for_login("user4"), url: "http://user4", likes: 0 }]

    # Create "likes" relations AFTER Users and Images
    UserImageLike.create [
        # User 1
        { user_id: id_for_login("user2"), image_id: id_for_url("http://user1") },
        # User 2
        { user_id: id_for_login("user3"), image_id: id_for_url("http://user2a") },
        { user_id: id_for_login("user4"), image_id: id_for_url("http://user2a") },
        { user_id: id_for_login("user1"), image_id: id_for_url("http://user2b") },
        # User 3
        { user_id: id_for_login("user1"), image_id: id_for_url("http://user3") },
        { user_id: id_for_login("user2"), image_id: id_for_url("http://user3") },
        { user_id: id_for_login("user4"), image_id: id_for_url("http://user3") }
    ]

    # Create "followers" relations AFTER Users
    FollowerRow.create [
        # User 1 follwers
        { user_id: id_for_login("user1"), follower_id: id_for_login("user2") },
        { user_id: id_for_login("user1"), follower_id: id_for_login("user3") },
        { user_id: id_for_login("user1"), follower_id: id_for_login("user4") },
        # User 2 follwers
        { user_id: id_for_login("user2"), follower_id: id_for_login("user3") },
        { user_id: id_for_login("user2"), follower_id: id_for_login("user4") },
        # User 3 follwers
        { user_id: id_for_login("user3"), follower_id: id_for_login("user4") }
    ]
end
