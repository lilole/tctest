
Rails.application.routes.draw do
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

    api_actions_basic = [:index, :show, :create, :update, :destroy]

    ##
     # The V1 API routes.
     #
    namespace :api do
        namespace :v1 do

            resources :images, only: api_actions_basic do
                post   'like',  on: :member                  # For current_user to like the given image
                delete 'like',  on: :member, action: :unlike # For current_user to unlike the given image
                get    'likes', on: :member                  # For users that have liked the given image
            end

            resources :users, only: api_actions_basic do
                post   'follow',    on: :member                    # For current_user to follow the given user
                delete 'follow',    on: :member, action: :unfollow # For current_user to unfollow the given user
                get    'following', on: :collection                # For current_user's followed users
                get    'followers', on: :collection                # For current_user's follower users
            end

            get '/feed/my',      to: "feed#my_feed"      # For current_user's followed users' recent images
            get '/feed/popular', to: "feed#popular_feed" # For all most liked images in past hour

        end
    end

end
