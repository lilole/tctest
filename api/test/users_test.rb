
require 'test_helper'

class UsersTest < ActiveSupport::TestCase

    test "user count" do
        assert_equal 5, User.count
    end

    test "user1 followers" do
        u = User.find_by(login: "user1")
        assert_not_nil u

        fs = u.followers
        assert_equal 2, fs.size
        assert_includes fs, User.find_by(login: "user2")
        assert_includes fs, User.find_by(login: "user3")
    end

    test "user1 following" do
        u = User.find_by(login: "user1")
        assert_not_nil u

        fs = u.following
        assert_equal 2, fs.size
        assert_includes fs, User.find_by(login: "user2")
        assert_includes fs, User.find_by(login: "user5")
    end

    test "user2 followers" do
        u = User.find_by(login: "user2")
        assert_not_nil u

        fs = u.followers
        assert_equal 3, fs.size
        assert_includes fs, User.find_by(login: "user1")
        assert_includes fs, User.find_by(login: "user3")
        assert_includes fs, User.find_by(login: "user4")
    end

    test "user2 following" do
        u = User.find_by(login: "user2")
        assert_not_nil u

        fs = u.following
        assert_equal 2, fs.size
        assert_includes fs, User.find_by(login: "user1")
        assert_includes fs, User.find_by(login: "user5")
    end

    test "user3 followers" do
        u = User.find_by(login: "user3")
        assert_not_nil u

        fs = u.followers
        assert_equal 0, fs.size
    end

    test "user3 following" do
        u = User.find_by(login: "user3")
        assert_not_nil u

        fs = u.following
        assert_equal 2, fs.size
        assert_includes fs, User.find_by(login: "user1")
        assert_includes fs, User.find_by(login: "user2")
    end

    test "user4 followers" do
        u = User.find_by(login: "user4")
        assert_not_nil u

        fs = u.followers
        assert_equal 0, fs.size
    end

    test "user4 following" do
        u = User.find_by(login: "user4")
        assert_not_nil u

        fs = u.following
        assert_equal 1, fs.size
        assert_includes fs, User.find_by(login: "user2")
    end

    test "user5 followers" do
        u = User.find_by(login: "user5")
        assert_not_nil u

        fs = u.followers
        assert_equal 2, fs.size
        assert_includes fs, User.find_by(login: "user1")
        assert_includes fs, User.find_by(login: "user2")
    end

    test "user5 following" do
        u = User.find_by(login: "user5")
        assert_not_nil u

        fs = u.following
        assert_equal 0, fs.size
    end

end
