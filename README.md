
# Original Requirements


>selfiegram
>----------
>
>Your friend, the quintessential idea guy, has an amazing idea for a new mobile app. Inspired by apps such as Instagram, he wants to build a hugely innovative derivative:
>
>Selfiegram! Just imagine it, it's like Instagram, but instead of taking pictures of everything else in the world, our killer app will take pictures of YOU! This changes everything.
>
>You'll be coding the back-end API for the mobile clients in Rails!
>
>Basic Features:
>
>  * Allow Users to submit images (just a URL field, no binary) with a caption field
>  * Allow Users to follow other users
>  * Allow Users to like/unlike other user posts
>
>Photo feed from the users you follow:
>
>  * Photo URL
>  * Owner
>  * Total number of likes on photo
>  * Total number of followers
>
>Popular feed where instead of seeing your followers, you'll see the most liked photos from any user in the past hour:
>
>  * Payload schema is the same as the photo feed.
>
>Deliverable should include:
>
>  * Include basic HTTP Auth.
>  * Include DB seed data.

# Implementation Notes

## Database
---

  * There are migrations in the standard place (`api/db/migrate/`).
  * The `api/db/seeds.rb` file creates test data to allow manually hitting the endpoints. It is commented.
    * NOTE: Default user logins and passwords are here, for HTTP Basic authn.
  * Care has been taken to create any necessary indexes and foreign keys.

## Routes
---

  * You can quickly see all the endpoints by checking `api/config/routes.rb`.

## Controllers
---

  * The base controller is at `api/app/controllers/api/v1/base_controller.rb`. It is fully documented.
  * Controllers with actions have action methods factored into an `Actions` module, to help with long-term maintenance.
  * Create/Update actions accept JSON that simply follows the fields defined in the corresponding object's table, found in its migration definition.

## Tests
---

  * Some automated tests were done. They can be run with `rails test -v`.
  * These tests exercise the followers/following logic.
  * Test file is `api/test/users_test.rb`.
  * Fixtures for these tests are at:
    * `api/test/fixtures/follower_rows.yml`
    * `api/test/fixtures/users.yml`
  * Other endpoints require manual testing with the `seed.rb` data, using Postman or similar to do raw JSON requests.
